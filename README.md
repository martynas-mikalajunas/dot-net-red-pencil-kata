# Source code for Red Pencil Kata

there you can find what we managed to do during TDD training/presentation

## Project structure

this is .net class library project (.dll)

Used packages:

- NUnit as test engine
- [FluentAssertions](https://github.com/dennisdoomen/fluentassertions/wiki) as assertion library
- We uesd [nCrunch](http://www.ncrunch.net/) to run our tests as we type

to see presentation and the kata description use following [link to presentation](http://martynas_mikalajunas.bitbucket.org)