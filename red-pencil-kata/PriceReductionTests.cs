﻿using System;
using FluentAssertions;
using NUnit.Framework;

namespace red_pencil_kata
{
    [TestFixture]
    public class PriceReductionTests
    {
        [TestCase(4, false, TestName = "nera raudonas piestukas kai kaina sumazejusi maziau 5 procentais")]
        [TestCase(5, true, TestName = "raudonas_piestukas_kai_kaina_sumazejusi_bent_5_procentais")]
        [TestCase(30, true, TestName = "raudonas_piestukas_kai_kaina_sumazejusi iki 30 procentu")]
        [TestCase(31, false, TestName = "nera raudonas_piestukas_kai_kaina_sumazejusi daugiau 30 procentu")]
        public void ReductionTest(int percent, bool hasHighlight)
        {
            var product = new Product(DateTime.UtcNow.AddDays(-30));
            product.SetDiscountInPercent(percent);
            product.PriceIsHighlighted().Should().Be(hasHighlight);
        }

        [TestCase(30, true, TestName = "raudonas piestukas kai kaina nekito bent 30 dienu")]
        [TestCase(29, false, TestName = "nera raudono piestuko kai kaina kito per 30 dienu")]
        public void StableDaysPriceTests(int stableDays, bool hasPromotion)
        {
            var product = new Product(DateTime.UtcNow.AddDays(-stableDays));
            product.SetDiscountInPercent(5);
            
            product.PriceIsHighlighted().Should().Be(hasPromotion);
        }

        [Test]
        public void Red_pencil_ends_after_30_days()
        {
            var product = Given_product_with_started_promotion_at(DateTime.UtcNow.AddDays(-31));
            product.PriceIsHighlighted().Should().BeFalse();
        }

        private Product Given_product_with_started_promotion_at(DateTime promotionStartDate)
        {
            var product = new Product(DateTime.UtcNow.AddDays(-30));
            product.SetDiscountInPercent(5, promotionStartDate);
            return product;
        }
    }

    public class Product
    {
        private int _discount;
        private bool _priceIsHighlighted;
        private DateTime _lastPriceDate;
        private DateTime _promotionStarted;

        public Product(DateTime priceDate)
        {
            _lastPriceDate = priceDate;
        }

        public void SetDiscountInPercent(int percent, DateTime? promotionStartDate = null)
        {
            _discount = percent;
            CheckForPromotion(promotionStartDate);
        }

        private void CheckForPromotion(DateTime? promotionStartDate)
        {
            var stableDays = (DateTime.UtcNow - _lastPriceDate).TotalDays;
            var discountOk = _discount >= 5 && _discount <= 30;

            if (!_priceIsHighlighted && discountOk && stableDays >= 30)
            {
                _priceIsHighlighted = true;
                _promotionStarted = promotionStartDate ?? DateTime.UtcNow;
            }
        }

        public bool PriceIsHighlighted()
        {
            var promotionDays = (DateTime.UtcNow - _promotionStarted).TotalDays;
            return _priceIsHighlighted && promotionDays <= 30;
        }
    }
}
